$(".burger").on("click", function (){
    var smallScreenNavigation = $(".small-screen-navigation");
    if(!$(this).hasClass("open")) {
        $(this).addClass("open");
        smallScreenNavigation.addClass("dropdown");
        smallScreenNavigation.show();
    } else  {
        $(this).removeClass("open");
        smallScreenNavigation.hide();
        smallScreenNavigation.removeClass("dropdown");
    }
});

$(window).on("resize", function (event) {
    var smallScreenNavigation = $(".small-screen-navigation");
    $(".burger").removeClass("open");
    smallScreenNavigation.hide();
    smallScreenNavigation.removeClass("dropdown");
});

const ctx = document.getElementById('informal-education-growth');
const myChart = new Chart(ctx, {
    type: 'line',
    responsive: true,
    data: {
        labels: ['2017', '2018', '2019', '2020', '2021'],
        datasets: [{
            label: 'growth in skills and informal knowledge',
            data: [10, 9, 13, 40, 80],
            borderColor: [
                'rgba(0, 0, 0, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

$("a").on("click", function(){
   alert();
});